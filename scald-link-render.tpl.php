<?php

/**
 * @file
 */
?>
<a href="<?php print $url; ?>" title="<?php print $title; ?>" target="_blank">
  <div class="atom-title"><?php print $title; ?></div>
  <div class="atom-link"><?php print $url; ?></div>
</a>
