<?php

/**
 * @file
 * Scald Link is a Scald Atom Provider for links.
 */

/**
 * Implements hook_scald_atom_providers().
 */
function scald_link_scald_atom_providers() {
  return array(
    'link' => 'Link',
  );
}

/**
 * Implements hook_scald_atom_providers_opt().
 *
 * Tell Scald that we don't need the 'add' step of the ctools wizard.
 */
function scald_link_scald_atom_providers_opt() {
  return array(
    'link' => array(
      'starting_step' => 'options',
    ),
  );
}

/**
 * Implements hook_scald_prerender().
 */
function scald_link_scald_prerender($atom, $context, $options, $mode) {
  if ($mode == 'atom') {
    $atom->rendered->player = array(
      '#theme' => 'scald_link_render',
      '#atom' => $atom,
      '#context' => $context,
      '#options' => $options,
    );
  }
}

/**
 * Implements hook_theme().
 */
function scald_link_theme() {
  return array(
    'scald_link_render' => array(
      'variables' => array(
        'atom' => NULL,
        'context' => NULL,
        'options' => NULL,
      ),
      'template' => 'scald-link-render',
    ),
  );
}

/**
 * Processes variables for scald_link_render.tpl.php.
 */
function scald_link_preprocess_scald_link_render(&$variables) {
  $atom = $variables['atom'];
  $link = field_get_items('scald_atom', $atom, 'scald_link');
  $variables['title'] = check_plain($link[0]['title'] ?: $atom->title);
  $variables['url'] = $link[0]['url'];
}
